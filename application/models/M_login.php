<?php 

    class M_login extends CI_Model {
        public function __construct()
        {
            $this->load->database();
            parent::__construct();
        }

        public function login_account($_username, $_password) {
            $user_is_exist = false;
            $_find_user  = $this->find_user($_username, $_password);
            if( $_find_user->num_rows() == 1 ){
                #user is exist
                $user_is_exist = true;
                #setting user session
                $this->load->library('session');
                #refresh user data if user has login before;
                $user_data = $_find_user->result_object();
                if( $this->session->has_userdata('account') ) 
                    $this->session->unset_userdata('account'); 
				$this->session->set_userdata('account',$user_data[0]);
            }
            return $user_is_exist;
        }

        public function find_user($userid, $password){ 
            $search_parameter = array('id' => $userid, 'password' => $password);
            $result = $this -> db -> select('*')
                                  -> from(RF_USER_TBL)
                                  -> where($search_parameter)
                                  -> get();
            return $result;
        }

      
      
    }

?>