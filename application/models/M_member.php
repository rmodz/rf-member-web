<?php 

    class M_member extends CI_Model {
        public function __construct()
        {
                // Call the CI_Model constructor
                $this->load->database();
                parent::__construct();
        }

        public function find_character($userid){
            #find character by user serial
            $WORLD_DB = $this->load->database('world', true);
            $search_param = array('Account'=> $userid, 'DCK'=>'0');
            return $WORLD_DB->get_where(RF_BASE_TBL, $search_param);
        }

        public function change_password($userid, $newpassword){
            $query = " UPDATE dbo.tbl_RFTestAccount
                       SET password = CONVERT(binary, {$this->db->escape($newpassword)} )
                       WHERE id = CONVERT(binary, {$this->db->escape($userid)})
                      ";
            return $this->db->simple_query($query);
        }

        public function get_account_trunk($account_serial){
            $WORLD_DB     = $this->load->database('world',true);
            $search_param = array('AccountSerial' => $account_serial);
            $query        = $WORLD_DB -> select('*')
                                      -> from(RF_ACCOUNT_TRUNK)  
                                      -> where($search_param)
                                      -> get();
            return $query;
        }

        public function set_account_trunk($account_serial, $newpass){
            $query = "  UPDATE dbo.tbl_AccountTrunk
                        SET TrunkPass = CONVERT(binary, {$this->db->escape($newpass)} )
                        WHERE AccountSerial = {$this->db->escape($account_serial)}
                     ";
            return $this->db->simple_query($query);
        }

        public function get_account_serial($user_id){
            $retval         = null;
            $WORLD_DB       = $this->load->database('world', true);
            $search_param   = array('Account' => $user_id );
            $query          = $WORLD_DB -> select('AccountSerial') 
                                        -> from(RF_BASE_TBL)
                                        -> where($search_param)
                                        -> get();
            if($query->num_rows() > 1){
                $account_serials = $query->result_object();
                $retval = $account_serials[0]->AccountSerial;
            }
            return $retval;
        }

        public function change_fg($account_serial, $newpassword){
            $query = "  UPDATE dbo.tbl_UserAccount
                        SET uilock_pw = CONVERT(binary, {$this->db->escape($newpassword)} )
                        WHERE serial = {$this->db->escape($account_serial)}
                     ";
            return $this->db->simple_query($query);
        }
    }

?>