<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {
	private $currentLoginUser;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_member');
		$this->load->library('session');
		$this->load->library('event_lib');
		$_account =  $this->session->userdata('account');
		$this->currentLoginUserId = $_account->id;
	}

	public function index()
	{
		$data['page_title'] = "Payment and event";
		$data['breadcrumb'] = array( array( 'link' => 'member', 
											'label' =>'Member Area', 
											'classes' => '' ), 
									 array( 'link' => 'member/payment-event', 
											'label' =>'Payment and event', 
											'classes' => 'active' ),
									 array( 'link' => 'member/payment-event', 
											'label' =>'Payment and event', 
											'classes' => 'active' )
									);
		$data['account'] 	= $this->session->userdata('account');
		// $this->load->view('Payment_home', $data); 
	}

	public function event_mapping(){
		$event_url  = $this->uri->segment(2);
		$event_info = $this->event_lib->get_detail_event($event_url);
		if( is_array($event_info) ):
			$data['event_data'] = $event_info;
			$this->load->view( 'events/'.$event_info['view'], $data);
		endif;
	}

}
