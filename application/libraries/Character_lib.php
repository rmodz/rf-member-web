<?php 
    class Character_lib
	{
	 
		function __construct()
		{
		}

		function rebuild_character($array_of_characters){
			$_characters = array();
			foreach($array_of_characters as $char):
				$_characters[] = array(
					'charSerial' => $char->Serial,
					'name' => $char->Name,
					'className' => $this->get_character_class( $char->Class ),
					'classCode' => $char->Class,
					'accSerial' => $char->AccountSerial,
					'raceCode' => $char->Race,
					'raceName' => $this->get_character_race($char->Race),
					'raceCurrency' => $this->get_character_currency($char->Race),
					'lvl' => $char->Lv,
					'balance' => $char->Dalant,
					'gold' => $char->Gold,
					'createTime' => $char->CreateTime,
				);  
			endforeach;
			return $_characters;
		}
	
		function get_character_class($classcode){
			$_classes = array(
				'BWB0' => 'Warrior', 
				'BWF1' => 'Commando',
				'BWF2' => 'Miller',
				'BWS1' => 'Berseker',
				'BWS2' => 'Armsman',
				'BWS3' => 'Shield Miller',
				'BRB0' => 'Ranger',
				'BRF1' => 'Desperado',
				'BRF2' => 'Sniper',
				'BRS1' => 'Hidden Soldier',
				'BRS2' => 'Sentinel',
				'BRS3' => 'Infiltrator',
				'BFB0' => 'Spritualist',
				'BFF1' => 'Psyper',
				'BFF2' => 'Chandra',
				'BFS1' => 'Wizard',
				'BFS2' => 'Astral',
				'BFS3' => 'Holy Chandra',
				'BSB0' => 'Specialist',
				'BSF1' => 'Craftsman',
				'BSF2' => 'Driver',
				'BSS1' => 'Mental Smith',
				'BSS2' => 'Armor Rider',
				'CWB0' => 'Warrior',
				'CWF1' => 'Champion',
				'CWF2' => 'Knights',
				'CWS1' => 'Templar',
				'CWS2' => 'Guardian',
				'CWS3' => 'Black Knights',
				'CRB0' => 'Ranger',
				'CRF1' => 'Archer',
				'CRF2' => 'Hunter',
				'CRS1' => 'Adventurer',
				'CRS2' => 'Steeler',
				'CRS3' => 'Assasin',
				'CFB0' => 'Spiritualist',
				'CFF1' => 'Caster',
				'CFF2' => 'Summoner',
				'CFS1' => 'Warlock',
				'CFS2' => 'Dark priest',
				'CFS3' => 'Grazier',
				'CSB0' => 'Specialist',
				'CSF1' => 'Craftsman',
				'CSS1' => 'Artist',
				'AWB0' => 'Warrior',
				'AWF1' => 'Destroyer',
				'AWF2' => 'Gladius',
				'AWS1' => 'Punisher',
				'AWS2' => 'Assaulter',
				'AWS3' => 'Mercenary',
				'ARB0' => 'Ranger',
				'ARF1' => 'Gunner',
				'ARF2' => 'Scouter',
				'ARS1' => 'Striker',
				'ARS2' => 'Dementer',
				'ARS3' => 'Phantom Shadow',
				'ASB0' => 'Specialist',
				'ASF1' => 'Engineer',
				'ASS1' => 'Scientist',
				'ASS2' => 'Battle Leader'
			);
	
			$_classname 	= 'Undefined class'; 
			if( array_key_exists($classcode, $_classes) ){
				$_classname = $_classes[$classcode];
			}
			return $_classname;
		}
	
		function get_character_race($raceCode){
			$_race = array(
				'4' => 'Accretia',
				'3' => 'Cora',
				'2' => 'Cora',
				'1' => 'Bellato',
				'0' => 'Bellato'
			);
	
			$_racename 	= 'Undefined Race'; 
			if( array_key_exists($raceCode, $_race) ){
				$_racename = $_race[$raceCode];
			}
			return $_racename;
		}
	
		function get_character_currency($raceCode){
			$_currency = array(
				'4' => 'C.P',
				'3' => 'Disena',
				'2' => 'Disena',
				'1' => 'Dalant',
				'0' => 'Dalant'
			);
	
			$_currencyName 	= 'Undefined Currency'; 
			if( array_key_exists($raceCode, $_currency) ){
				$_currencyName = $_currency[$raceCode];
			}
			return $_currencyName;
		}
	}
?>
