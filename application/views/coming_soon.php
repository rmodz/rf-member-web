<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
 	<?php include('template/_header_member.php');  ?>
	<!-- editable section -->
    <div id="spacer" style="padding-top:5em;"></div>
    <div class="container theme-showcase" role="main">
      <!-- Main jumbotron for a primary marketing message or call to action -->

      <div class="container">
        <div class="row">
            <!-- header area -->
            <div class="col-xs-12">
              <div class="page-header">
                <h1><?php echo $page_title; ?> <small></small></h1>
              </div>
              <div class="breadcrumb">
                <?php foreach($breadcrumb as $bc) :
                        $link = site_url($bc['link']);
                        echo "<li> <a href='{$link}' class='{$bc['classes']}'> {$bc['label']} </a> </li>";
                      endforeach; ?>
                <div class="pull-right"> server status : <b><a href="#" style="color: green" >online</a></b> </div>
              </div>
            </div>
            <!-- ./header area -->
            <!-- left menu area -->
            <?php include('template/_menu_member.php') ?>
            <!-- .left menu area -->
            <div class="col-xs-9">
              <h4> Coming soon</h4>
              <!-- right area -->
              <div class="alert alert-warning" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  this feature is under development.
                </div>
              
            </div>
          <!-- .right area -->
        </div>
      </div>

    </div>
	<!-- ./ editable section -->
  <script> 
    var amountStr = document.getElementsByClassName('currency');
    var goldStr   = document.getElementsByClassName('gold');
   
    const strToThousand = (_strAmount) => {
      let _strFormated = _strAmount;
      let sisa 	  = _strAmount.length % 3;
      let rupiah 	= _strAmount.substr(0, sisa);
      let ribuan 	= _strAmount.substr(sisa).match(/\d{3}/g);

      if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
        _strFormated = rupiah;
      }
      return _strFormated;
    }

    Array.from(amountStr).forEach( (amount, idx) => {
      amountStr[idx].innerText = strToThousand(amount.innerText);
    });

    Array.from(goldStr).forEach( (amount, idx) => {
      goldStr[idx].innerText = strToThousand(amount.innerText);
    });
   

  </script>
 	<?php include('template/_footer_member.php');  ?>
</html>
