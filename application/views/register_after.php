<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
 	<?php include('template/_header.php');  ?>
	<!-- editable section -->
	<body>

	<div class="container">

	<div class="form-signin">
			<div class="bs-example" data-example-id="simple-panel"> 
				<div class="panel panel-default"> 
					<div class="panel-body"> 
						<div class="alert alert-success" role="alert"> <strong>Lets Play !</strong> </br>
							Your account successfully created. 
						</div>
					</div> 
					<div class="panel-footer">
						<a class="btn btn-primary" href="<?php echo site_url('member') ?>"> Login </a>
					</div>
				</div> 
			</div>
	</div>

	</div> <!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
	<script src='assets/js/login.js'></script>
	</body>


	<!-- ./ editable section -->
 	<?php include('template/_footer.php');  ?>
</html>
