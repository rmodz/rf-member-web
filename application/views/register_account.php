<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
 	<?php include('template/_header.php');  ?>
	<!-- editable section -->
	<body>

	<div class="container">

	<form class="form-signin" id="form_register" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<div class="row" style="max-width: 480px;">
			<div class="panel panel-default">
				<div class="panel-heading heading-2"></div>
				<div class="panel-body">
					<div class="text-center">
						<img class="panel-profile-img" src="assets/image/rf_logo">
						<p class="author-title"><strong>Register</strong></p>
						<p class="author-text">Complete the field bellow to register.</p>
					</div>
					<?php if( $this->session->flashdata('fail_register') == true ): ?>
						<div class="alert alert-danger" role="alert">Sorry, Account with that username already registered.</div>
					<?php endif; ?>
				</div>
				<div class="panel-footer">
					<div class="spacer"></div>
					<div class="row">
						<div class="col-md-8 col-xs-12">
							<div class="form-group">
								<!-- <label>Username</label> -->
								<input 	type="text" 
										id="inputEmail" 
										name="username" 
										class="form-control input-xs" 
										placeholder="Username" 
										minlength="4"
										value=""
										required autofocus />
							</div>
							<div class="form-group">
								<!-- <label for="inputPassword">Password</label> -->
								<input type="password" id="pw" name="password" class="form-control" placeholder="Password" value="" required>
							</div>
						</div>
						<div class="col-md-4" style="padding-top: 0.5em">
							<button class="btn btn-success btn-block" 
									style="height: 67px;"
									type="submit"
									name="submit">
								Create Account
							</button>
						</div>
					</div>
					<p>Already have account? <a href="<?php echo site_url('/login')?>">Login now.</a></p>
				</div>
			</div>
		</div>
	</form>

	</div> <!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
	<script src='assets/js/login.js'></script>
	</body>


	<!-- ./ editable section -->
 	<?php include('template/_footer.php');  ?>
</html>
