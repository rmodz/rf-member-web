<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
 	<?php include('template/_header_member.php');  ?>
	<!-- editable section -->
    <div id="spacer" style="padding-top:5em;"></div>
    <div class="container theme-showcase" role="main">
      <!-- Main jumbotron for a primary marketing message or call to action -->

      <div class="container">
        <div class="row">
            <!-- header area -->
            <div class="col-xs-12">
              <div class="page-header">
                <h1><?php echo $page_title; ?> <small></small></h1>
              </div>
              <div class="breadcrumb">
                <?php foreach($breadcrumb as $bc) :
                        $link = site_url($bc['link']);
                        echo "<li> <a href='{$link}' class='{$bc['classes']}'> {$bc['label']} </a> </li>";
                      endforeach; ?>
                <div class="pull-right"> server status : <b><a href="#" style="color: green" >online</a></b> </div>
              </div>
            </div>
            <!-- ./header area -->
            <?php include('template/_menu_member.php') ?>
         
            <div class="col-xs-9">
              <div class="col-xs-12">
                <?php if(!isset($submit_error) ): ?>
                <div class="alert alert-warning" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <strong>Warning</strong>
                  <br/>
                  This action will change your  bank password to <u>random generate number</u>, please change the password in game after restore.
                </div>
                <?php else: ?>
                <div class="alert alert-danger" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <strong>Error ! </strong><?php echo $submit_error; ?>
                </div>
                <?php endif; ?>
                <?php if(isset($submit_success) ): ?>
                <div class="alert alert-success" role="alert">
                  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                  <?php echo $submit_success;?>
                </div>
                <?php endif; ?>
              </div>
              <!-- right area -->
              <form method="post"  action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div class="col-xs-5">
                  <div class="form-group">
                      <label> Enter Account password </label>
                      <input  type="password" 
                              class="form-control" 
                              value="" 
                              name="current_password"
                              required/>
                  </div>
                  <div class="form-group">
                  <p>
                    <input type="checkbox" aria-label="..." name="agree" value="true" required>
                     I agree to change my bank password
                  </p>
                  </div>
                 
                  <div class="form-group">
                      <button type="submit" name="submit" class="btn btn-primary">Generate bank password</button>
                  </div>
                </div>
              </form>
            </div>
          <!-- .right area -->
        </div>
      </div>

    </div>
	<!-- ./ editable section -->
  
 	<?php include('template/_footer_member.php');  ?>
</html>
