<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
 	<?php include('template/_header_member.php');  ?>
	<!-- editable section -->
    <div id="spacer" style="padding-top:5em;"></div>
    <div class="container theme-showcase" role="main">
      <!-- Main jumbotron for a primary marketing message or call to action -->

      <div class="container">
        <div class="row">
            <!-- header area -->
            <div class="col-xs-12">
              <div class="page-header">
                <h1><?php echo $page_title; ?> <small></small></h1>
              </div>
              <div class="breadcrumb">
                <?php foreach($breadcrumb as $bc) :
                        $link = site_url($bc['link']);
                        echo "<li> <a href='{$link}' class='{$bc['classes']}'> {$bc['label']} </a> </li>";
                      endforeach; ?>
                <div class="pull-right"> server status : <b><a href="#" style="color: green" >online</a></b> </div>
              </div>
            </div>
            <!-- ./header area -->
            <!-- left menu area -->
            <?php include('template/_menu_member.php') ?>
            <!-- .left menu area -->
            <div class="col-xs-9">
              <h4><i class="glyphicon glyphicon-user"></i> Character List</h4>
              <!-- right area -->
             
              <?php if( count($characters) < 1 ): ?>
                <div class="alert alert-info" role="alert">Look like you're doesnt have any character, please create one .</div>
              <?php else: ?>
              <?php foreach($characters as $char): ?>
                <div class="panel" style="border-color: #245580">
                  <div class="panel-body">
                    <div class="col-md-2 text-center" >
                      <img src="assets/image/<?php echo $char['classCode']; ?>.webp" style="max-width:90px; valign:middle">
                    </div>
                    <div class="col-md-10">
                      <div class="col-md-4">
                        <h3><strong><?php echo $char['name']; ?></strong></h3>
                        <p><?php echo $char['raceName']." ".$char['className'] ?></p>
                      </div>
                      <div class="col-md-8" style="padding-top:1.5em;">
                        <table class="table table-condensed">
                          <tr>
                            <td class="col-xs-2"> <?php echo $char['raceCurrency'] ?> </td>
                            <td class="col-xs-10"> <p class="currency"> <?php echo $char['balance'] ?>  </p> </td>
                          </tr>
                          <tr>
                            <td class="col-xs-2"> Gold </td>
                            <td class="col-xs-10"> <p class="gold"> <?php echo $char['gold'] ?>  </p> </td>
                          </tr>
                        </table>
                      </div>
                      <div class="col-md-6">

                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach; endif; ?>
            </div>
          <!-- .right area -->
        </div>
      </div>

    </div>
	<!-- ./ editable section -->
  <script> 
    var amountStr = document.getElementsByClassName('currency');
    var goldStr   = document.getElementsByClassName('gold');
   
    const strToThousand = (_strAmount) => {
      let _strFormated = _strAmount;
      let sisa 	  = _strAmount.length % 3;
      let rupiah 	= _strAmount.substr(0, sisa);
      let ribuan 	= _strAmount.substr(sisa).match(/\d{3}/g);

      if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
        _strFormated = rupiah;
      }
      return _strFormated;
    }

    Array.from(amountStr).forEach( (amount, idx) => {
      amountStr[idx].innerText = strToThousand(amount.innerText);
    });

    Array.from(goldStr).forEach( (amount, idx) => {
      goldStr[idx].innerText = strToThousand(amount.innerText);
    });
   

  </script>
 	<?php include('template/_footer_member.php');  ?>
</html>
